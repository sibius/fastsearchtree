# FastSearchTree

## Sample project for demonstration of the custom storage with the fast content lookup (wildacrd*). 

### Implementation
Data structure - tree with two types of nodes represented with classes: **Node** and **WordEndNode**.   
**Storage** class encapsulates the tree functionality rpviding interface to the client with the following methods:    
1. int GetWordsCount(string startingWith); 
2. IEnumerable<string> GetWords(string startingWith); 
3. void AddWord(string word, int id); 

### Methods complexity
1. GetWordsCount - **O(m)** where m is the length of the prefix to search (can be considered O(1), does not depend on number of words added to the storage)
2. GetWords - **O(n*m)** where n is limited number (returned by Node.CountWordsAfter) and m is average word length stored in the sub-tree.

(sample tree illustration is provided bellow).


![alt text](http://178.128.192.175:8099/tree.jpg "Data structure (sample)")    
    
###TODO:  
Caching can be added to improve GetWords method's performance.  
