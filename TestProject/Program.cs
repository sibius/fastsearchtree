﻿using System;
using System.Collections.Generic;
using TestProject.Tree;


namespace TestProject
{
    class Program
    {
        /*
         * 
         * The Storage class encapsulates the following data structure (based on sample inputData):
        
             (*)
            /   \
          (A)    (B)
          /  \      \
        (L)   (N)    (R)
        / \     \      \
      (B)  (E)   (D)    (I)
      /\     \     \      \
  4:[Y] (E)  [X]:1  (R)    (A)
         |      \     \      \
        (R)     (A)   (E)     [N]:6
         |       |      \
        [T]:3   (N)     [W]:2
                 |
                (D)
                 |
                [R]:5

        *
        * Terminating nodes are represented in square brackets. They also store the ID of corresponding stored word.
        * Regular nodes are represented with round brackets. Each node contains field (_wordsDown) with pre-calculated value of words bellow (in the subtree)
        */

        static void Main(string[] args)
        {
            #region Sample input data
            var initData = new Dictionary<int, string>()
            {
                { 1, "ALEX" },
                { 2, "ANDREW" },
                { 3, "ALBERT" },
                { 4, "ALBY" },
                { 5, "ALEXANDER" },
                { 6, "BRIAN" },
            };
            #endregion

            Storage db = new Storage();
            foreach(var item in initData)
            {
                db.AddWord(item.Value, item.Key);
            }            

            string[] tests = new string[] { "A", "AZ", "AN", "ALB", "ALE", "B", "C" };

            foreach(string test in tests)
            {
                int matches = db.GetWordsCount(test);
                IEnumerable<string> words = db.GetWords(test);               

                Console.WriteLine($"Testing input: {test}");
                Console.WriteLine($"Number of matches: {matches}");
                Console.WriteLine($"Found words: {string.Join(" ", words)}\n");                
            }

            Console.ReadKey();
        }
    }

   

    
}
