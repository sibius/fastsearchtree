﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.Tree
{
    interface INode
    {
        int CountWordsAfter();                  //get number of words stored in the sub-tree of the node
        IEnumerable<string> GetWordsAfter();    //get list of words stored in the sub-tree of the node    
        INode FindChild(char value);            //find the first matching node in the tree by value (character)
        INode AddChild(char value);             //save the character in the tree
        INode AddWordEndChild(char value, int id);  //save the character which is the last in the word  (ID is an external id of the word)
    }

    class Node : INode  
    {
        protected Dictionary<char, Node> _children; 
        protected Node _parent;
        protected char _value;     //character of a word
        protected int _wordsDown;  //calculated on adding new word into the storage/tree (see method AddWordEndChild)

        #region ctor
        public Node(char value)
        {
            _value = value;
            _children = new Dictionary<char, Node>();
        }
        #endregion

        public int CountWordsAfter()  //complexity: O(1)
        {
            return _wordsDown;
        }

        public IEnumerable<string> GetWordsAfter() //complexity is O(n*m) where n is limited number (returned by CountWordsAfter) and m is average word length stored in the sub-tree.
        {
            StringBuilder head = new StringBuilder();
            Node n = this._parent;
            while (n != null && n._value != '*')
            {
                head.Insert(0, n._value);
                n = n._parent;
            }

            IEnumerable<StringBuilder> tails = GetWordsStartingFrom(this);
            List<string> words = new List<string>();
            foreach (var tail in tails)
                words.Add(head.ToString() + tail.ToString());

            return words;
        }        

        public INode FindChild(char value)
        {
            Node found;
            if (_children.TryGetValue(value, out found))
                return found;
            return null;
        }

        public INode AddChild(char value)
        {
            Node n = new Node(value);
            AddChild(n);
            return n;
        }

        public INode AddWordEndChild(char value, int id)
        {
            Node n = new WordEndNode(value, id);
            AddChild(n);
            Node parent = this;
            while (parent != null)
            {
                parent._wordsDown += 1;
                parent = parent._parent;
            }
            n._wordsDown += 1;
            return n;
        }

        protected void AddChild(Node child)
        {
            child._parent = this;
            _children.Add(child._value, child);
        }

        #region service method(s)
        private IEnumerable<StringBuilder> GetWordsStartingFrom(Node n)
        {
            List<StringBuilder> res = new List<StringBuilder>();
            if (n is WordEndNode)
            {
                var sb = new StringBuilder();
                sb.Append(n._value);
                res.Add(sb);
            }
            foreach (var kvp in n._children)
            {
                IEnumerable<StringBuilder> tails = GetWordsStartingFrom(kvp.Value);
                foreach (StringBuilder s in tails)
                {
                    s.Insert(0, n._value);
                    res.Add(s);
                }
            }
            return res;
        }
        #endregion
    }


    class WordEndNode : Node  //Terminating Node (end of the word). Every word is composed by linked nodes between root and the terminating node. 
    {
        public int ID { get; set; }  //external identifier

        public WordEndNode(char value, int id) : base(value)
        {
            ID = id;
        }
    }
}
