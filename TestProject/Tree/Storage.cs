﻿using System.Collections.Generic;

namespace TestProject.Tree
{
    class Storage
    {
        Node _root;
        public Storage()
        {
            _root = new Node('*');
        }

        public void AddWord(string word, int id)  //saving the word in the tree along with its external identifier 
        {            
            if (string.IsNullOrEmpty(word))
                return;
            INode node = _root;
            for (int i = 0; i < word.Length; i++)
            {
                INode foundChild = node.FindChild(word[i]);
                if (foundChild != null)
                {
                    node = foundChild;
                    continue;
                }
                if (i == word.Length - 1)
                    node = node.AddWordEndChild(word[i], id);
                else
                    node = node.AddChild(word[i]);
            }
        }

        public int GetWordsCount(string startWith)   //complexity: O(m)  where m is the length of the sub-string to search (the prefix length)
        {
            var endNode = FindWordSubTree(startWith);
            return endNode != null ? endNode.CountWordsAfter() : 0;
        }

        public IEnumerable<string> GetWords(string startWith)  //complexity is O(n*m) where n is limited number (returned by CountWordsAfter) and m is average word length stored in the sub-tree.
        {
            //TODO: caching can be added (read here) e.g. from MemCache or from external storage like Redis (with DI) 
            var endNode = FindWordSubTree(startWith);
            return endNode != null ? endNode.GetWordsAfter() : new string[0];
        }

        private INode FindWordSubTree(string word)
        {
            INode node = _root;
            if (string.IsNullOrEmpty(word))
                return null;
            for (int i = 0; i < word.Length; i++)
            {
                INode foundChild = node.FindChild(word[i]);
                if (foundChild == null)
                    return null;
                node = foundChild;
            }
            return node;
        }
    }
}
